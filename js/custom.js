// Tab Js
jQuery(function($) {
    $('.dm-page-card-tab ul li ').click(function() {
        var a = $(this);
        var active_tab_class = 'selected';
        var the_tab = '.' + a.attr('data-tab');
        $('.dm-page-card-tab ul li ').removeClass(active_tab_class);
        a.addClass(active_tab_class);
        $('.dm-tab-info .tabs').removeClass('is-active');
        $(the_tab).addClass('is-active');
        return false;
    });
});

// Defult color 
// $('input[type="color"]').val('#FE71FF');

// Enable Disable
$('.dm-enable-disble-btn').click(function() {
    if ($(this).hasClass('is-disble')) {
        $(this).removeClass('is-disble');
        $(this).find('.btn-content').text('enable');
    } else {
        $(this).addClass('is-disble');
        $(this).find('.btn-content').text('disable');
    }
});


// Fb Popup
$('.dm-fb-page').click(function() {
    $('.dm-facebook-page-popup').addClass('open-popup');
    $('.dm-fb-popup-content').addClass('popup-active-animation');
    $('.dm-fb-popup-content').removeClass('popup-diactive-animation ');
});
$('.popup-close-icon').click(function() {
    $('.dm-facebook-page-popup').removeClass('open-popup');
    $('.dm-fb-popup-content').removeClass('popup-active-animation');
    $('.dm-fb-popup-content').addClass('popup-diactive-animation');
});


// Submit Loader
$('.submit-btn.btn-success').click(function() {
    $('.btn-success').addClass('submited');
    $('.btn-success').text('submited...');
    setTimeout(function() {
        $('.btn-success').removeClass('submited');
        $('.btn-success').text('Submit');
        $('.success-message-text').addClass('is-save-successfully');
        setTimeout(function() {
            $('.success-message-text').removeClass('is-save-successfully');
        }, 1500);
    }, 5000);
});

$('.submit-btn.button-error').click(function() {
    $(this).addClass('btn-error');
    $(this).text('error!');
    setTimeout(function() {
        $('.submit-btn').removeClass('btn-error');
        $('.submit-btn').text('Submit');
    }, 1500);
});


// Menu Drawer

$(".menu-bar").hover(function() {
    $(".main-menu-drawer").addClass('open-menu-drawer ');
}, function() {
    $(".main-menu-drawer").removeClass('open-menu-drawer ');
});

$('.dm-menu-bar li .menu-title a').click(function() {
    $('.dm-menu-bar li.menu-active').removeClass('menu-active');
    $(this).parents('li').addClass('menu-active');
});